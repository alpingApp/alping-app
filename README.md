# Alping

![Pipeline](https://gitlab.com/alpingApp/alping-app/badges/main/pipeline.svg)

## Description

Alping is a web platform that aims to connect people and hiking enthusiasts.

- Users can create their own trekking routes and share them with the community.
- Interested people can look for original tracks and contact their creators in order to meet and organize the trip together.
- There is probably a track for every skill or difficulty !

**The web platform is available here : [ALPING](https://alping.web.app/)**

## Visuals

![Home page](./img/visual1.png)

![Search Hikes](./img/visual2.png)

## Installation

> This project uses NodeJS 16.13.0 and Vue.js

**Project setup**

1. First the repo must be cloned inside a local folder :

```git
git clone https://gitlab.com/alpingApp/alping-app.git
```

2. Then, `.env` files must be provided to the frontend and backend. Contact the developpers to obtain the `.env` files. The folder `frontend` and `backend` must each have a `.env` file inside.

In order to run the platform locally, three components must be started.

**The Mongo database**

A local docker container with MongoDB needs to be set up.

3. Start by creating a `docker-compose.yaml` file containing the following code :

```yaml
version: '3.1'
services:
  mongo:
    image: mongo
    container_name: "mongo-alping"
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: admin
      MONGO_INITDB_ROOT_PASSWORD: alping
    volumes:
      - './dockervolume/mongodb:/data/db'
    ports:
      - 27017:27017
```

4. Then open a terminal where the `docker-compose.yaml` is located and run the following command to start the MongoDB container :

```bash
docker-compose up
```

**The backend API**

Once MongoDB is runnning, the Alping backend API can be started.

5. Open a terminal inside the `backend` folder.

6. Run the `npm install` command inside the folder

7. Start the backend server with the command `npm run start`. The server should start and automatically connect to MongoDB

**The frontend Vue.js app**

Finally, the frontend web app can be started

8. Open a terminal inside the backend folder

9. Run the `npm install` command inside the folder

10. Start the frontend server with the command `npm run serve`. The server should start and automatically and be available on [http://localhost:8080/](http://localhost:8080/) 

> Tip : the backend / frontend tests can be started with `npm run test`

## Usage

**Use case**

An initial use case has been created in order to clarify the web application usage :

<img src="img/useCase.jpg" alt="useCase" style="zoom: 50%;" />

**Base Functionnalities**

- Register

- Login

- Edit profile

- Create hikes

- Edit hikes

- Add hike to favorites

- Search hikes

## Production structure

> Edit: the backend API has been moved to [render.com](https://render.com/). The structure schema was not updated.

The platform is fully deployed when a new release is created. 
The deployment pushes the frontend on a Firebase hosting service and the backend on ~~a Heroku service~~ render.com.

![](./img/structure.png)

## Support

If there is any problem, contact a member of the team :

- daniel.sciarra@heig-vd.ch
- marco.maziero@heig-vd.ch
- alessandro.parrino@heig-vd.ch
- hoanganh.mai@heig-vd.ch
- gaetan.zwick@heig-vd.ch

## Roadmap

The project is organizer on 4 main sprints.

- Sprint 0 : Project setup and DevOps

- Sprint 1 : Basic routing and setup of data structures for the API

- Sprint 2 : Completed frontend pages and available endpoints in backend

- Sprint 3 : Link between frontend and backend API. Release of version 1.0

> More details on the sprints in the tab [Milestones · Alping / Alping-app · GitLab](https://gitlab.com/alpingApp/alping-app/-/milestones)

## Contributing

You can contribute by proposing new issues or creating a new MR on existing issues. The code will then be reviewed by a developper of the team. If everything looks good, the MR will be merged in the `main` branch. 

## Authors and acknowledgment

The team members are : 

- Daniel Sciarra
- Marco Maziero
- Alessandro Parrino
- Hoang Anh Mai
- Gaétan Zwick

## License

[MIT License](./LICENSE)

## Project status

Currently in active development.
