## What
<!--
A brief explanation of the why, not the what or how. Assume the reader doesn't know the
background and won't have time to dig-up information from comment threads.
-->


## Relevant links
<!--
Information that the developer might need to refer to when implementing the issue.

- [Design Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/<id>)
  - [Design 1](https://gitlab.com/gitlab-org/gitlab/-/issues/<id>/designs/<image>.png)
  - [Design 2](https://gitlab.com/gitlab-org/gitlab/-/issues/<id>/designs/<image>.png)
- [Similar implementation](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/<id>)
-->

## Implementation plan
<!--
Steps and the parts of the code that will need to get updated. The plan can also
call-out responsibilities for other team members or teams.

e.g.:

- [ ] ~frontend Step 1
  - [ ] `@person` Step 1a
- [ ] ~frontend Step 2

-->


<!--
Workflow and other relevant labels

# ~"group::" ~"Category:" ~"GitLab Ultimate"
Other settings you might want to include when creating the issue.

# /assign @
# /epic &
-->

/label ~"workflow::refinement"
/milestone %Backlog
