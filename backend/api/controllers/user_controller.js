const User = require("../../models/user");

/**
 * Retrieves a user by its id.
 * @param {*} req request
 * @param {*} res response
 * @param {*} next
 */
const getUserByUid = async (req, res, next) => {
    try {
        let user = await User.findOne({ 'uid': req.params.uid })
        res.status(200).json(user._doc);
    } catch (error) {
        res.status(400).send('Error during fetch operation. The user was not found.');
    }
}

/**
 * Retrieves a user by its username.
 * @param {*} req request
 * @param {*} res response
 * @param {*} next
 */
const getUserByUsername = async (req, res, next) => {
    try {
        let user = await User.findOne({ 'username': req.params.username })
        res.status(200).json(user._doc);
    } catch (error) {
        res.status(400).send('Error during fetch operation. The user was not found.');
    }
}

/**
 * Add a user to the DB.
 * @param {*} req request
 * @param {*} res response
 * @param {*} next
 */
const addUser = async (req, res, next) => {
    try {
        let newUser = new User(req.body.user);
        if (await User.exists({'email': newUser.email})) {
            res.status(300).send('Error during create operation. The email is already registered');
            return;
        }
        if (await User.exists({'username': newUser.username})) {
            res.status(300).send('Error during create operation. The username already exists');
            return;
        }
        await newUser.save();
        res.status(200).send('User created successfully');
    } catch (error) {
        res.status(400).send('Error during create operation. The user was not created.');
    }
}

/**
 * Updates a user in the database
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
const updateUser = async (req, res, next) => {
    try {
        if (req.body.user === undefined) {
            res.status(400).send('Missing user object in request body');
            return;
        }

        const id = req.params.username;
        await User.findOneAndUpdate({ 'uid': req.params.uid }, req.body.user);
        res.status(200).send('User updated successfully');
    } catch (error) {
        res.status(400).send('Error during update operation. User ID was probably not found.');
    }
}

/**
 * Deletes a given user
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
const deleteUser = async (req, res, next) => {
    try {
        await User.findOneAndDelete({ 'uid': req.params.uid })
        res.status(200).send('User deleted successfully');
    } catch (error) {
        res.status(400).send('Error during fetch operation. The user was not found nor deleted.');
    }
}

/**
 * Update a user's list of favorites hikes.
 * @param {*} req request
 * @param {*} res response
 * @param {*} next
 */
const updateUserFavorites = async (req, res, next) => {
    try {
        if (req.body.favorites === undefined) {
            res.status(400).send('Missing favorites object in request body');
            return;
        }
        let user = await User.findOneAndUpdate({ 'uid': req.params.uid }, { favorites: req.body.favorites });
        res.status(200).send('Update successful.');
    } catch (error) {
        res.status(400).send('Error during update operation.');
    }
}

/**
 * Update a user's list of created hikes.
 * @param {*} req request
 * @param {*} res response
 * @param {*} next
 */
const updateUserCreated = async (req, res, next) => {
    try {
        if (req.body.hikes === undefined) {
            res.status(400).send('Missing hikes object in request body');
            return;
        }
        let user = await User.findOneAndUpdate({ 'uid': req.params.uid }, { createdHikes: req.body.hikes });
        res.status(200).send('Update successful.');
    } catch (error) {
        res.status(400).send('Error during update operation.');
    }
}

module.exports = {
    getUserByUid,
    getUserByUsername,
    addUser,
    updateUserFavorites,
    updateUserCreated,
    updateUser,
    deleteUser,
}
