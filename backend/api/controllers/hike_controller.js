const Hike = require('../../models/hike');
const Location = require('../../models/location');
const https = require('https');
const querystring = require('querystring');

/**
 * Adds a new hike in the db
 * @param req HTTP request object, containing the hike
 * @param res HTTP response object
 * @returns {Promise<void>}
 */
const addHike = async (req, res) => {
  try {
    if (req.body.hike === undefined) {
      res.status(400).send('Missing hike object in request body');
      return;
    }
    const newHike = new Hike(req.body.hike);
    await newHike.save();
    res.status(200).send(newHike._id.toString());
  } catch (error) {
    res.status(400).send('Error during create operation. The hike was not created.');
  }
}

/**
 * Retrieves all hikes from db
 * @param req HTTP request object
 * @param res HTTP response object
 * @returns {Promise<void>}
 */
const getAllHikes = async (req, res) => {
  try {
    const data = await Hike.find()
    if (data.empty) {
      res.status(404).send('No hike record found');
    } else {
      res.send(data);
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
}

/**
 * Retrieves a specific hike in the db
 * @param req HTTP request object, containing the hike's id
 * @param res HTTP response object
 * @returns {Promise<void>}
 */
const getHike = async (req, res) => {
  try {
    const id = req.params.id;
    const data = await Hike.findById(id);
    if (!data) {
      res.status(404).send('Hike with the given ID not found');
    } else {
      res.send(data)
    }
  } catch (error) {
    res.status(400).send(error.message);
  }
}

/**
 * Deletes a specific hike in the db
 * @param req HTTP request object, containing the hike's id
 * @param res HTTP response object
 * @returns {Promise<void>}
 */
const deleteHike = async (req, res) => {
  try {
    const id = req.params.id;
    await Hike.findByIdAndDelete(id)
    res.status(200).send('Hike deleted successfully');
  } catch (error) {
    res.status(400).send('Error during delete operation. Hike ID was probably not found.');
  }
}

/**
 * Updates a specific hike in the db
 * @param req HTTP request object, containing the hike info
 * @param res HTTP response object
 * @returns {Promise<void>}
 */
const updateHike = async (req, res) => {
  try {
    if (req.body.hike === undefined) {
      res.status(400).send('Missing hike object in request body');
      return;
    }

    const id = req.params.id;
    await Hike.findByIdAndUpdate(id, req.body.hike);
    res.status(200).send('Hike updated successfully');
  } catch (error) {
    res.status(400).send('Error during update operation. Hike ID was probably not found.');
  }
}

/**
 * Retrieves hikes for a given search fields with coordinates location
 * Query parameters are :
 * Date       : datemin, datemax
 * Region     : latitude, longitude, rad
 * Duration   : durmin, durmax
 * Distance   : distmin, distmax
 * Difficulty : diffmin, diffmax
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const searchHikesWithCoords = async (req, res) => {
  // Retrieves and validates query parameters
  if (req.query.datemin === undefined ||
    req.query.datemax === undefined ||
    req.query.latitude === undefined ||
    req.query.longitude === undefined ||
    req.query.rad === undefined ||
    req.query.durmin === undefined ||
    req.query.durmax === undefined ||
    req.query.distmin === undefined ||
    req.query.distmax === undefined ||
    req.query.diffmin === undefined ||
    req.query.diffmax === undefined) {
    res.status(400).send('Missing parameters in search query. Required parameters: ' +
      'datemin, datemax, place, rad, durmin, durmax, distmin, distmax, diffmin, diffmax');
    return;
  }

  const foundHikes = await searchHikes(req.query.datemin,
    req.query.datemax,
    req.query.latitude,
    req.query.longitude,
    req.query.rad,
    req.query.durmin,
    req.query.durmax,
    req.query.distmin,
    req.query.distmax,
    req.query.diffmin,
    req.query.diffmax
  );

  if (foundHikes === null) {
    res.status(400).send("An error occurred while searching hikes")
  } else {
    res.send(foundHikes);
  }
}

/**
 * Retrieves hikes for a given search fields with place name location
 * Query parameters are :
 * Date       : datemin, datemax
 * Region     : place
 * Duration   : durmin, durmax
 * Distance   : distmin, distmax
 * Difficulty : diffmin, diffmax
 * @param req
 * @param res
 * @returns {Promise<void>}
 */
const searchHikesWithPlace = async (req, res) => {
  // Retrieves and validates query parameters
  if (req.query.datemin === undefined ||
    req.query.datemax === undefined ||
    req.query.place === undefined ||
    req.query.rad === undefined ||
    req.query.durmin === undefined ||
    req.query.durmax === undefined ||
    req.query.distmin === undefined ||
    req.query.distmax === undefined ||
    req.query.diffmin === undefined ||
    req.query.diffmax === undefined) {
    res.status(400).send('Missing parameters in search query. Required parameters: ' +
      'datemin, datemax, place, rad, durmin, durmax, distmin, distmax, diffmin, diffmax');
    return;
  }

  let coords = {
    latitude: 46.779,
    longitude: 6.659,
  }; // default to HEIG-VD

  if (req.query.place === '') {
    // Place is center of switzerland w/ big radius
    req.query.place = 'Switzerland';
    req.query.rad = '2000';
  }

  try {
    // Converts the place to coordinates using OpenStreetMap Nominatim API
    coords = await getCoordinates(req.query.place)
    const foundHikes = await searchHikes(
      req.query.datemin,
      req.query.datemax,
      coords.latitude,
      coords.longitude,
      req.query.rad,
      req.query.durmin,
      req.query.durmax,
      req.query.distmin,
      req.query.distmax,
      req.query.diffmin,
      req.query.diffmax
    );
    if (foundHikes === null) {
      res.status(400).send("An error occurred while searching hikes")
    } else {
      res.send({
        location: {lat: coords.latitude, lng: coords.longitude},
        hikes: foundHikes
      });
    }
  } catch (e) {
    res.status(400).send(e)
  }
}

/**
 * Uses the Google API to convert a textual pace name into coordinates
 * @param place The place to find
 */
async function getCoordinates(place) {
  // Checks if the place is in cache
  const strPlace = place.toLowerCase();
  if (await Location.exists({name: strPlace})) {
    const location = await Location.findOne({name: strPlace});
    return Promise.resolve({
      latitude: location.latitude,
      longitude: location.longitude,
    });
  }

  const params = {
    q: place,
    countrycodes: 'ch,fr',
    format: 'jsonv2',
    limit: 1
  };
  const url = `https://nominatim.openstreetmap.org/search?${querystring.stringify(params)}`;
  const options = {
    headers: {
      'User-Agent': 'Alping v.1.0 (https://gitlab.com/alpingApp/alping-app)',
    }
  };

  return new Promise((resolve, reject) => {
    https.get(url, options, res => {
      let body = '';

      res.on('data', chunk => {
        body += chunk
      });

      res.on('end', () => {
        if (res.statusCode === 404) {
          reject('Location not found');
          return;
        }
        // Parses the received data and returns the coordinates
        let parsedBody = null;
        try {
          parsedBody = JSON.parse(body);
          if (parsedBody.length === 0) {
            reject('Location not found');
          } else {
            const {lat, lon} = parsedBody[0];
            // Stores the new location in cache
            const newLocation = new Location({
              name: strPlace,
              latitude: lat,
              longitude: lon,
            });
            newLocation.save();

            resolve({
              latitude: lat,
              longitude: lon,
            });
          }
        } catch (e) {
          reject('Unable to use received location');
        }
      });
    }).on('error', () => {
      reject('Error when retrieving location coordinates');
      return null;
    });
  });
}

/**
 * Util function that searches hikes matching given parameters
 * @param datemin The min range for the hike start date
 * @param datemax The max range for the hike start date
 * @param latitude The hike latitude
 * @param longitude The hike longitude
 * @param rad The hike radius
 * @param durmin The hike min duration
 * @param durmax The hike max duration
 * @param distmin The hike min distance
 * @param distmax The hike max distance
 * @param diffmin The hike min difficulty
 * @param diffmax The hike max difficulty
 */
async function searchHikes(datemin, datemax, latitude, longitude, rad, durmin, durmax, distmin, distmax, diffmin, diffmax) {
  // Runs the query
  try {
    rad = parseFloat(rad) / 111.0 // Converts km to degrees
    return Hike.find({
      date: {$gte: new Date(datemin), $lte: new Date(datemax)},
      duration: {$gte: parseFloat(durmin), $lte: parseFloat(durmax)},
      distance: {$gte: parseFloat(distmin), $lte: parseFloat(distmax)},
      difficulty: {$gte: parseInt(diffmin), $lte: parseInt(diffmax)},
      'location.latitude': {$gte: (parseFloat(latitude) - rad), $lte: (parseFloat(latitude) + rad)},
      'location.longitude': {$gte: (parseFloat(longitude) - rad), $lte: (parseFloat(longitude) + rad)}
    });
  } catch (error) {
    return null
  }
}

module.exports = {
  addHike,
  getAllHikes,
  getHike,
  deleteHike,
  updateHike,
  searchHikesWithCoords,
  searchHikesWithPlace,
}
