const express = require("express");
const {
  addHike,
  getAllHikes,
  getHike,
  deleteHike,
  updateHike,
  searchHikesWithCoords,
  searchHikesWithPlace,
} = require("../controllers/hike_controller");
const slowDown = require("express-slow-down");
const {jwtCheck} = require("../../utils/auth");

const router = express.Router();
// protect routes with Auth0 token
router.use(jwtCheck)

// Common routes
router.post("/", addHike);
router.get("/", getAllHikes);
router.get("/:id", getHike);
router.delete("/:id", deleteHike);
router.put("/:id", updateHike);

// Search filters routes
// Query parameters are :
// Date       : datemin, datemax
// Region     : latitude, longitude, rad
// Duration   : durmin, durmax
// Distance   : distmin, distmax
// Difficulty : diffmin, diffmax
router.get("/search/coords", searchHikesWithCoords);

// Rate-limit for the geocoding service. It limits api calls frequency to 1x/s.
const speedLimiter = slowDown({
  windowMs: 1000, // 1s
  delayAfter: 1, // allow 1 requests to go at full-speed, then...
  delayMs: 1000 // further requests have a 1000ms delay
});

// Search filters routes
// Query parameters are :
// Date       : datemin, datemax
// Region     : place
// Duration   : durmin, durmax
// Distance   : distmin, distmax
// Difficulty : diffmin, diffmax
router.get("/search/place", speedLimiter, searchHikesWithPlace);

module.exports = {
  routes: router,
};
