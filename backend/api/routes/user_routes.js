const express = require("express");
const {
    getUserByUid,
    getUserByUsername,
    addUser,
    getAllUsers,
    updateUserFavorites,
    updateUserCreated,
    updateUser,
    deleteUser,
} = require("../controllers/user_controller");
const {jwtCheck} = require("../../utils/auth");

const router = express.Router();
// protect routes with Auth0 token
router.use(jwtCheck)

router.post("/", addUser);
router.put("/:uid", updateUser);
router.delete("/:uid", deleteUser);
router.get("/:uid", getUserByUid);
router.get("/username/:username", getUserByUsername);
router.put("/favorites/:uid", updateUserFavorites);
router.put("/hikes/:uid", updateUserCreated);

module.exports = {
    routes: router,
}
