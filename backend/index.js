const dotenv = require('dotenv');
dotenv.config(); // initialize env variables
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
// needs to be called after env variables initialization
const {getAPIToken} = require("./utils/auth");
const hikeRoutes = require('./api/routes/hike_routes');
const userRoutes = require('./api/routes/user_routes');
const port = process.env.PORT || 3000;
const app = express();

// Cors options
app.use(cors({
  credentials: true,
  origin: [
    process.env.ALPING_URL,
    'https://alping-frontend.web.app',
    'https://maps.googleapis.com',
    'https://nominatim.openstreetmap.org',
  ],
  optionsSuccessStatus: 200
}));
app.options('*', cors());

app.use(express.json());

app.get('/api/token', async (req, res) => {
  res.send(await getAPIToken());
})
app.use('/api/hike', hikeRoutes.routes);
app.use('/api/user', userRoutes.routes);

app.listen(port, () => {
  console.log(`Alping API listening on port ${port}`);
})

// Set up mongoose connection
function connect() {
  mongoose.connect(process.env.ALPING_MONGO_URI, {useNewUrlParser: true, useUnifiedTopology: true});
}

// Initiate the connexion
if (process.env.NODE_ENV !== 'test') {
  connect();
  setTimeout(function () {
    connect();
  }, 60000);
}

mongoose.connection.on('error', console.error.bind(console, 'MongoDB connection error:'));
mongoose.connection.on('connected', () => console.log('Connected to MongoDB'));

module.exports = app
