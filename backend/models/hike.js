const mongoose = require('mongoose');

const HikeSchema = new mongoose.Schema({
  title: String,
  description: String,
  image: String,
  owner: String,
  location: {
    latitude: Number,
    longitude: Number
  },
  distance: Number,
  duration: Number,
  difficulty: Number,
  date: Date,
  participants: [String]
});

const Hike = mongoose.model('Hike', HikeSchema);

module.exports = Hike;
