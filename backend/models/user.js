const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  uid: String,
  username: String,
  email: String,
  phone: String,
  experience: Number,
  image: String,
  favorites: [String],
  createdHikes: [String]
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
