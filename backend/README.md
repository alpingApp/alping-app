# Alping - Backend server

![Pipeline](https://gitlab.com/alpingApp/alping-app/badges/main/pipeline.svg)

## Description

The backend API is hosted on [Render.com](https://render.com/) and works with a NodeJS Express solution.
The database is hosted on MongoDB Atlas.

## Documentation

The full API documentation is available at : [https://app.swaggerhub.com/apis-docs/alping/Alping/](https://app.swaggerhub.com/apis-docs/alping/Alping/)

## Installation

**The Mongo database**

A local docker container with MongoDB needs to be set up.

1. Start by creating a `docker-compose.yaml` file containing the following code :

```yaml
version: '3.1'
services:
  mongo:
    image: mongo
    container_name: "mongo-alping"
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: admin
      MONGO_INITDB_ROOT_PASSWORD: alping
    volumes:
      - './dockervolume/mongodb:/data/db'
    ports:
      - 27017:27017
```

2. Then open a terminal where the `docker-compose.yaml` is located and run the following command to start the MongoDB container :

```bash
docker-compose up
```

**The backend API**

3. Once MongoDB is runnning, the Alping backend API can be started.

4. Open a terminal inside the `backend` folder.

5. Run the `npm install` command inside the folder

6. Start the backend server with the command `npm run start`. The server should start and automatically connect to MongoDB
