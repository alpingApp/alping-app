const { auth } = require('express-oauth2-jwt-bearer');
const fetch = require('node-fetch');

/**
 * Checks JWT validity.
 */
const jwtCheck = auth({
  audience: process.env.ALPING_AUTH0_AUDIENCE,
  issuerBaseURL: process.env.ALPING_AUTH0_DOMAIN,
  tokenSigningAlg: 'RS256'
});

/**
 * Retrieves a token from Auth0 to authorize further API calls.
 * @returns {Promise<{access_token: string}>} the token.
 */
const getAPIToken = async () => {
  const token = {access_token: ''}
  const response = await fetch(process.env.ALPING_AUTH0_URI, {
    method: 'POST',
    body: JSON.stringify({
      client_id: process.env.ALPING_AUTH0_CLIENT_ID,
      client_secret: process.env.ALPING_AUTH0_SECRET,
      audience: process.env.ALPING_AUTH0_AUDIENCE,
      grant_type: 'client_credentials',
    }),
    headers: {'Content-Type': 'application/json'},
  });
  if (response.ok) {
    const res = await response.json();
    token.access_token = res.access_token;
  }
  return token;
}

module.exports = {jwtCheck, getAPIToken}
