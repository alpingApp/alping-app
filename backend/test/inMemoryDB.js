const mongoose = require('mongoose');
const {MongoMemoryServer} = require('mongodb-memory-server');

let mongoServer;
const mongooseOpts = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
};

/**
 * Connect to the in-memory database.
 */
const connect = async () => {
  await mongoose.disconnect(); // close previous connection
  mongoServer = await MongoMemoryServer.create();
  const uri = mongoServer.getUri();
  await mongoose.connect(uri, mongooseOpts, err => {
    console.log('Connecting to inMemoryDB')
    if (err) {
      console.error(err);
    }
  });
}

/**
 * Close the connection and stop mongod.
 */
const closeDatabase = async () => {
  await mongoose.disconnect();
  await mongoServer.stop();
}

module.exports = {connect, closeDatabase}