process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const server = require('../index');
const {getAPIToken} = require("../utils/auth");
const inMemoryDB = require('./inMemoryDB')
chai.use(chaiHttp);

let user = {
    uid: "1abd3e22ejqwdja12dw",
    username: "JohnDoe",
    email: "johndoe@mail.com",
    phone: "1234567890",
    experience: 3,
    image: "3022022.jpg",
    favorites: [0, 1, 2],
    createdHikes: [2, 3, 4, 5]
}
let AuthToken;

// Tests the user endpoints
describe('Test USERS endpoints', () => {

    before(async () => {
      await inMemoryDB.connect();
      AuthToken = await getAPIToken();
    });

    after(async () => {
      await inMemoryDB.closeDatabase();
    })

    // POST
    describe('/POST /api/user', () => {
        it('it should create test user', (done) => {
            chai.request(server)
                .post('/api/user')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"user": user})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('User created successfully');
                    done();
                });
        });

        it('it should tell a user already exists with this email', (done) => {
            chai.request(server)
                .post('/api/user')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"user": user})
                .end((err, res) => {
                    res.should.have.status(300);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Error during create operation. The email is already registered');
                    done();
                });
        });

        it('it should tell a user already exists with the same username', (done) => {
            user.email = 'new@email.com';
            chai.request(server)
                .post('/api/user')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"user": user})
                .end((err, res) => {
                    res.should.have.status(300);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Error during create operation. The username already exists');
                    done();
                });
        });
    });

    // GET
    describe('/GET /api/user', () => {
        it('it should get the user by uid', (done) => {
            chai.request(server)
                .get(`/api/user/${user.uid}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.uid.should.be.eq(user.uid);
                    done();
                });
        });

        it('it should find no user with non-existent uid', (done) => {
            chai.request(server)
                .get('/api/user/unexistinguiduser')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Error during fetch operation. The user was not found.');
                    done();
                });
        });

        it('it should get the user by username', (done) => {
            chai.request(server)
                .get(`/api/user/username/${user.username}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.uid.should.be.eq(user.uid);
                    done();
                });
        });

        it('it should find no user with non-existent username', (done) => {
            chai.request(server)
                .get('/api/user/username/wrongusername298hwsqd982ds8912olk')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Error during fetch operation. The user was not found.');
                    done();
                });
        });
    });

    // PUT
    describe('/PUT /api/user', () => {
        it('it should update the user phone number', (done) => {
            chai.request(server)
                .put(`/api/user/${user.uid}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"user": {"phone": "0987654321"}})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('User updated successfully');
                    done();
                });
        });

        it('it should update a user favorites list', (done) => {
            chai.request(server)
                .put(`/api/user/favorites/${user.uid}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"favorites": [7, 8, 9]})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Update successful.');
                    done();
                });
        });

        it('it should update a user created hikes list', (done) => {
            chai.request(server)
                .put(`/api/user/hikes/${user.uid}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"hikes": [11, 22, 33]})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Update successful.');
                    done();
                });
        });

        it('it should not update a user created hikes list when hikes object not specified', (done) => {
            chai.request(server)
                .put(`/api/user/hikes/${user.uid}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Missing hikes object in request body');
                    done();
                });
        });
    });

    // DELETE
    describe('/DELETE /api/user', () => {
        it('it should delete the test user', (done) => {
            chai.request(server)
                .delete(`/api/user/${user.uid}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('User deleted successfully');
                    done();
                });
        });
    });
});
