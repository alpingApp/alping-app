process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
const server = require('../index');
const {getAPIToken} = require("../utils/auth");
const inMemoryDB = require('./inMemoryDB')
chai.use(chaiHttp);

// Defines test hikes
let hike1ID;
let hike1 = {
    title: 'Hike in Lausanne',
    description: 'Just an example',
    image: '2',
    owner: 0,
    location: {
        name: 'Lausanne',
        latitude: 46.519964,
        longitude: 6.633592
    },
    distance: 20,
    duration: 2.6,
    difficulty: 2,
    date: new Date('1970-01-01T00:00:00.000Z'),
    participants: [1, 2, 3]
};

let hike2ID;
let hike2 = {
    title: 'Hike in Zurich',
    description: 'Just an example',
    image: '1',
    owner: 1,
    location: {
        name: 'Zurich',
        latitude: 47.3769,
        longitude: 8.5417
    },
    distance: 10,
    duration: 1.2,
    difficulty: 1,
    date: new Date('1970-02-01T00:00:00.000Z'),
    participants: [0, 2, 3]
};

let AuthToken;

// Tests the hikes endpoints
describe('Test HIKES endpoints', () => {

    before(async () => {
      await inMemoryDB.connect();
      AuthToken = await getAPIToken();
    });

    after(async () => {
      await inMemoryDB.closeDatabase();
    })

    // POST
    describe('/POST /api/hike', () => {
        it('it should create test hike in Lausanne', (done) => {
            chai.request(server)
                .post('/api/hike')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"hike": hike1})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    done();
                });
        });

        it('it should create test hike in Zurich', (done) => {
            chai.request(server)
                .post('/api/hike')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"hike": hike2})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    done();
                });
        });

        it('it should return an error message when the body does not contain hike object', (done) => {
            chai.request(server)
                .post('/api/hike')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({})
                .end((err, res) => {
                    res.should.have.status(400);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Missing hike object in request body')
                    done();
                });
        });
    });

    // GET
    describe('/GET /api/hike', () => {
        it('it should get all stored hikes', (done) => {
            chai.request(server)
                .get('/api/hike')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.greaterThanOrEqual(2);
                    done();
                });
        });

        it('it should get the hike in Lausanne only', (done) => {
            chai.request(server)
                .get('/api/hike/search/place?datemin=1970-01-01&datemax=1970-01-02&place=Lausanne&rad=5&durmin=0&durmax=500&distmin=10&distmax=30&diffmin=1&diffmax=5')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.hikes.should.be.a('array');
                    res.body.hikes.length.should.greaterThanOrEqual(1);
                    let foundLausanne = false;
                    let foundZurich = false;
                    res.body.hikes.forEach(e => {
                        if (e.title === 'Hike in Zurich') {
                            foundZurich = true;
                        } else if (e.title === 'Hike in Lausanne') {
                            foundLausanne = true;
                            hike1ID = e._id;
                        }
                    });
                    foundZurich.should.be.eq(false);
                    foundLausanne.should.be.eq(true);
                    done();
                });
        });

        it('it should get the hike in Zurich only', (done) => {
            chai.request(server)
                .get('/api/hike/search/coords?datemin=1970-01-01&datemax=1970-02-06&latitude=47.3769&longitude=8.5417&rad=5&durmin=0&durmax=500&distmin=10&distmax=30&diffmin=1&diffmax=5')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.greaterThanOrEqual(1);
                    let foundLausanne = false;
                    let foundZurich = false;
                    res.body.forEach(e => {
                        if (e.title === 'Hike in Zurich') {
                            foundZurich = true;
                            hike2ID = e._id;
                        } else if (e.title === 'Hike in Lausanne') {
                            foundLausanne = true;
                        }
                    });
                    foundZurich.should.be.eq(true);
                    foundLausanne.should.be.eq(false);
                    done();
                });
        });

        it('it should get no hike', (done) => {
            chai.request(server)
                .get('/api/hike/search/place?datemin=1982-03-05&datemax=1982-03-06&place=Paris&rad=5&durmin=0&durmax=500&distmin=10&distmax=30&diffmin=4&diffmax=5')
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.hikes.should.be.a('array');
                    res.body.hikes.should.be.empty;
                    done();
                });
        });
    });

    // PUT
    describe('/PUT /api/hike', () => {
        it('it should update the hike in Lausanne', (done) => {
            chai.request(server)
                .put(`/api/hike/${hike1ID}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .send({"hike": {"title": "Updated hike in Lausanne!"}})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Hike updated successfully');
                    done();
                });
        });
    });

    // DELETE
    describe('/DELETE /api/hike', () => {
        it('it should delete the hike in Lausanne', (done) => {
            chai.request(server)
                .delete(`/api/hike/${hike1ID}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Hike deleted successfully');
                    done();
                });
        });

        it('it should delete the hike in Zurich', (done) => {
            chai.request(server)
                .delete(`/api/hike/${hike2ID}`)
                .set({"Authorization": `Bearer ${AuthToken.access_token}`})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.text.should.be.a('string');
                    res.text.should.be.eq('Hike deleted successfully');
                    done();
                });
        });
    });
});
