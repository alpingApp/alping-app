import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import {store} from './store'
import VueRellax from 'vue-rellax'
import * as VueGoogleMaps from 'vue2-google-maps'
import './assets/scss/main.scss'
import "@/plugins/firebase";

Vue.use(VueRellax)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBzvUksXBtSM6CRwfENVQRGST0mW_vhieg',
  },
  installComponents: true,
});
Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
