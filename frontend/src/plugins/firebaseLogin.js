import {
    createUserWithEmailAndPassword,
    getAuth,
    GoogleAuthProvider,
    onAuthStateChanged,
    signInWithEmailAndPassword,
    signInWithPopup,
    signOut
} from "firebase/auth";
import {userProfilePicture} from "@/tools/apiInteraction";
import {addUserToDB, fetchUserByUid} from "@/tools/dbInteraction";
import router from "@/router";

export function firebaseSignUp(userData, commit) {
    createUserWithEmailAndPassword(
        getAuth(),
        userData.email,
        userData.password
    )
        .then((userCredentials) => {
            const user = {
                uid: userCredentials.user.uid,
                username: userData.username,
                email: userData.email,
                phone: userData.phone,
                experience: userData.experience,
                image: userProfilePicture(userData.username),
                favorites: [],
                createdHikes: []
            };
            addUserToDB(user);
            commit('setUser', user);
            router.replace({name: 'Home'});
        })
        .catch((error) => {
            commit('setSigningUp', false);
            console.log(error.code, error.message);
            alert(error.message);
        });
}

export function firebaseSignIn(userData, dispatch) {
    signInWithEmailAndPassword(
        getAuth(),
        userData.email,
        userData.password
    )
        .then((userCredentials) => {
            const user = userCredentials.user;
            dispatch('setUserInStore', user.uid);
            router.replace({name: 'Home'});
        })
        .catch(() => {
            alert('Utilisateur inconnu, veuillez vous enregistrer.');
        });
}

export function firebaseGoogleSignIn(commit) {
    const provider = new GoogleAuthProvider();
    signInWithPopup(getAuth(), provider)
        .then((userCredentials) => {
            const gUser = userCredentials.user;
            fetchUserByUid(gUser.uid)
                .then((user) => {
                    if (user === null) {
                        user = {
                            uid: gUser.uid,
                            username: gUser.displayName,
                            email: gUser.email,
                            phone: gUser.phoneNumber,
                            experience: 1,
                            image: gUser.photoURL,
                            favorites: [],
                            createdHikes: []
                        };
                        addUserToDB(user);
                    }
                    commit('setUser', user);
                })
                .catch((error) => console.log(error));

            router.replace({name: 'Home'});
        })
        .catch((error) => {
            console.log(error);
        });
}

export function firebaseLogout(commit) {
    signOut(getAuth())
        .then(() => {
            commit('setUser', null);
            router.replace({name: 'Home'});
        })
        .catch((error) => {
            console.log(error);
        });
}

export function firebaseOnAuthStateChanged(state, commit) {
    onAuthStateChanged(getAuth(), user => {
        if (user && state.user === null) {
            if (state.signingUp) { // avoid "user = null" after signUp
                commit('setSigningUp', false);
            } else {
                // fetching user after a refresh
                fetchUserByUid(user.uid)
                    .then((userDB) => {
                        commit('setUser', userDB);
                    })
                    .catch((error) => console.log(error));
            }
        }
    });
}
