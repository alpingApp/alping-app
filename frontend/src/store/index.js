import Vue from 'vue'
import Vuex from 'vuex'
import {
  updateUserHikes,
  fetchUserByUid,
  getAPIToken,
  updateUserFavorites,
  updateHikeParticipants,
} from '../tools/dbInteraction';
import {
  firebaseSignUp,
  firebaseSignIn,
  firebaseGoogleSignIn,
  firebaseLogout,
  firebaseOnAuthStateChanged
} from '../plugins/firebaseLogin'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null,
    signingUp: false,
    apiJwt: null,
    hikeToUpdate: null,
  },
  getters: {
    userFavoritesHikes: state => {
      if (state.user) {
        return state.user.favorites
      } else {
        return null
      }
    },
    userCreatedHikes: state => {
      if (state.user) {
        return state.user.createdHikes
      } else {
        return null
      }
    },
    isUserAuth: state => {
      return state.user !== null
    },
    connectedUser: state => {
      return state.user
    },
    getApiToken: state => {
      return state.apiJwt
    },
    hikeToUpdate: state => {
      return state.hikeToUpdate
    },
  },
  mutations: {
    addHikeToFav(state, hike) {
      state.user.favorites = [...state.user.favorites, hike]
    },
    removeHikeFromFav(state, id) {
      state.user.favorites = state.user.favorites.filter(hikeId => hikeId !== id);
    },
    addHikeToCreated(state, hike) {
      state.user.createdHikes = [...state.user.createdHikes, hike]
    },
    setUser(state, user) {
      state.user = user
    },
    setSigningUp(state, status) {
      state.signingUp = status
    },
    setAPIToken(state, token) {
      state.apiJwt = token;
    },
    setHikeToUpdate(state, hike) {
      state.hikeToUpdate = hike
    },
  },
  actions: {
    addCreatedHike({ commit, state }, hikeId) {
      commit('addHikeToCreated', hikeId)
      // Update DB
      updateUserHikes(
        state.user.uid,
        state.user.createdHikes
      );
    },

    setUserInStore({ commit }, uid) {
      fetchUserByUid(uid)
        .then((userDB) => {
          commit('setUser', userDB);
        })
        .catch((error) => console.log(error));
    },

    addFavHike({ commit, state }, {hikeId, participants}) {
      commit('addHikeToFav', hikeId)
      // Update DB
      updateUserFavorites(
        state.user.uid,
        state.user.favorites
      );
      updateHikeParticipants(hikeId, participants);
    },

    removeFavHike({ commit, state }, {hikeId, participants}) {
      commit('removeHikeFromFav', hikeId)
      // Update DB
      updateUserFavorites(
        state.user.uid,
        state.user.favorites
      );
      updateHikeParticipants(hikeId,participants);
    },

    signUpAction({ commit }, userData) {
      commit('setSigningUp', true);
      firebaseSignUp(userData, commit);
    },

    signInAction({ dispatch }, userData) {
      firebaseSignIn(userData, dispatch);
    },

    googleSignInAction({ commit }) {
      firebaseGoogleSignIn(commit);
    },

    signOutAction({ commit }) {
      firebaseLogout(commit);
    },

    authAction({ commit, state }) {
      firebaseOnAuthStateChanged(state, commit);
    },

    APIToken({ commit, state }) {
      if (state.apiJwt === null || Date.now() >= state.apiJwt.expires_in * 1000) {
        return getAPIToken()
            .then((token) => {
              commit('setAPIToken', token)
              return store.state.apiJwt
            })
            .catch((error) => console.log(error));
      }
      return Promise.resolve(state.apiJwt);
    },
  },
})
