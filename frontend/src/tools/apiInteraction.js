/**
 * return the url for the profile picture API
 * @param seed
 * @returns {string}
 */
export function userProfilePicture(seed) {
  return `https://avatars.dicebear.com/api/adventurer-neutral/${seed}.svg?radius=50`
}
