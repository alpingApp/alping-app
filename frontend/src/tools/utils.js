const QUERY_TYPE = {
  ALL: 'all',
  QUERY: 'query'
}

// default query data
const DEFAULT_QUERY = {
  datemin: '2000-01-01',
  datemax: '2999-12-31',
  place: 'Bern',
  distmin: 0,
  distmax: 100,
  diffmin: 1,
  diffmax: 4,
}

/**
 * Search on /api/hike
 * @param context
 */
function searchAll(context) {
  context.$router.push({
    name: 'Search',
    query: {type: QUERY_TYPE.ALL},
  }).catch(() => {});
}

/**
 * Search on /api/hike/query?...
 * @param context
 * @param queryData
 * @param rad
 */
function searchData(context, queryData, rad = 150) {
  queryData.rad = rad;
  context.$router.push({
    name: 'Search',
    query: Object.assign({type: QUERY_TYPE.QUERY}, queryData),
  }).catch(() => {});
}

/**
 * formate the date (from ISO to dd.mm.yyyy)
 * @param date
 * @returns {string}
 */
function formatDate(date) {
  return `${date.substring(8,10)}.${date.substring(5,7)}.${date.substring(0,4)}`;
}

export {searchAll, searchData, formatDate, DEFAULT_QUERY, QUERY_TYPE}
