/**
 * format the hike to post
 * @param hikeToPost
 * @returns {{hike: {owner: (null|number|*), duration: number, difficulty: (number|*), date: Date, image, distance: number, description, location: {latitude, longitude}, title, participants: *[]}}}
 */
export function formatHikeToPost(hikeToPost) {
    return {
        hike: {
            title: hikeToPost.title,
            description: hikeToPost.description,
            image: hikeToPost.image,
            owner: hikeToPost.owner,
            location: {
                latitude: hikeToPost.location.lat,
                longitude: hikeToPost.location.lng,
            },
            distance: parseFloat(hikeToPost.distance),
            duration: parseFloat(hikeToPost.duration),
            difficulty: hikeToPost.difficulty,
            date: new Date(hikeToPost.date),
            participants: [],
        },
    };
}

/**
 * for update hike participant
 * @param hikeToPost
 * @returns {{hike: {owner: (number|*|null), duration: number, difficulty: (number|*), date: Date, image, distance: number, description, location: {latitude, longitude}, title, participants: *[]}}}
 */
export function formatUpdatedHikeToPost(hikeToPost) {
    let hikeDTO = formatHikeToPost(hikeToPost);
    hikeDTO.hike._id = hikeToPost._id;
    hikeDTO.hike.participants = hikeToPost.participants;
    console.log(hikeDTO)
    return hikeDTO;
}

/**
 * return a hike from a hike from db
 * @param dbHike
 * @returns {{owner: (null|number|*), duration: string, difficulty: (number|*), date: *, image, distance: string, description, location: {lng, lat}, _id: *, title, participants: ([]|number[]|[String | StringConstructor]|*)}}
 */
export function formatHike(dbHike) {
    return {
        _id: dbHike._id,
        title: dbHike.title,
        description: dbHike.description,
        image: dbHike.image,
        owner: dbHike.owner,
        location: {
            lat: dbHike.location.latitude,
            lng: dbHike.location.longitude
        },
        distance: dbHike.distance.toString(),
        duration: dbHike.duration.toString(),
        difficulty: dbHike.difficulty,
        date: dbHike.date.split('T')[0],
        participants: dbHike.participants
    }
}
