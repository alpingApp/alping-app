const emailRules = [
  input => !!input || "Email est requis.",
  input => /^[\w-.]+@([\w-]+\.)+[\w-]{2,4}$/.test(input) || 'Email non valide.',
];
const pwdRules = [
  input => !!input || "Le mot de passe est requis.",
  input => /.{6,}/.test(input) || 'Le mot de passe doit contenir au moins 6 caractères.',
];
const phoneRules = [
  input => /^[0-9]{10}$/.test(input) || 'Le numéro de téléphone doit contenir 10 chiffres.',
];
const textRules = [
  input => /[a-zA-z]+/.test(input) || 'Caractères autorisés : lettres.',
];
const numberRules = [
  input => /^[0-9]+\.?[0-9]*$/.test(input) || 'Veuillez entrer un nombre.',
];
const titleRules = [
  (input) => /[a-zA-z]+/.test(input) || 'Caractères autorisés : lettres',
  (input) => /^.{1,50}$/.test(input) || 'Minimum 1 caractère, maximum 50 caractères'
]

export { emailRules, pwdRules, phoneRules, textRules, numberRules, titleRules}
