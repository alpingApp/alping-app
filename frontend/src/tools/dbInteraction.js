import {formatHikeToPost, formatUpdatedHikeToPost} from "./hikeMapper";
import {store} from "@/store";
import {DEFAULT_QUERY} from "./utils";

const API_URI = process.env.VUE_APP_ALPING_API_URI

/**
 * fetch user by its uid
 * @param uid
 * @returns {Promise<null|any>}
 */
export async function fetchUserByUid(uid) {
  const token = await store.dispatch('APIToken')
  const response = await fetch(`${API_URI}/user/${uid}`, {
    headers: {'Authorization': `Bearer ${token.access_token}`},
  });
  if (!response.ok) {
    return null;
  }
  return await response.json();
}

/**
 * fetch user by its username and push error page if not found
 * @param context
 * @param username
 * @returns {Promise<any>}
 */
export async function fetchUserByUsername(context, username) {
  const token = await store.dispatch('APIToken');
  try {
    const response = await fetch(`${API_URI}/user/username/${username}`, {
      headers: {'Authorization': `Bearer ${token.access_token}`},
    });
    if (!response?.ok) {
      await context.$router.push('/error/404');
      return;
    }
    return await response.json();
  } catch (e) {
    await context.$router.push('/error/500');
  }
}

/**
 * add user to DB
 * @param user
 * @returns {Promise<boolean>}
 */
export async function addUserToDB(user) {
  const token = await store.dispatch('APIToken')
  const response = await fetch(`${API_URI}/user/`, {
    method: 'POST',
    body: JSON.stringify({user: user}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token.access_token}`
    },
  });
  return response.ok;
}

/**
 * udpate hike from user (uid)
 * @param uid
 * @param createdHikes
 * @returns {Promise<boolean>}
 */
export async function updateUserHikes(uid, createdHikes) {
  const token = await store.dispatch('APIToken');
  const response = await fetch(`${API_URI}/user/hikes/${uid}`, {
    method: 'PUT',
    body: JSON.stringify({hikes: createdHikes}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token.access_token}`
    },
  });
  return response.ok;
}

/**
 * update the user favorites when a new hike is added or removed
 * @param uid
 * @param favorites
 * @returns {Promise<boolean>}
 */
export async function updateUserFavorites(uid, favorites) {
  const token = await store.dispatch('APIToken')
  const response = await fetch(`${API_URI}/user/favorites/${uid}/`, {
    method: 'PUT',
    body: JSON.stringify({favorites: favorites}),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token.access_token}`
    },
  });
  return response.ok;
}

/**
 * update user (uid) data
 * @param uid
 * @param data new data
 * @returns {Promise<boolean>}
 */
export async function updateUserData(uid, data) {
  const token = await store.dispatch('APIToken')
  const response = await fetch(`${API_URI}/user/${uid}/`, {
    method: 'PUT',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token.access_token}`
    },
  });
  if (response.ok) {
    await store.dispatch('setUserInStore', uid)
  }
  return response.ok;
}

/**
 * fetch hike from a list of hike ids
 * @param hikeIdList
 * @returns {Promise<*[]>} list of hikes
 */
export async function fetchHikes(hikeIdList) {
  const token = await store.dispatch('APIToken')
  const hikes = []
  for (const hikeId of hikeIdList) {
    const response = await fetch(`${API_URI}/hike/${hikeId}`, {
      headers: {'Authorization': `Bearer ${token.access_token}`},
    });
    if (response.ok) {
      const hike = await response.json();
      hikes.push(hike);
    }
  }
  return hikes
}

/**
 * post a new hike to the DB
 * @param hike
 * @returns {Promise<boolean>}
 */
export async function postHike(hike) {
  const token = await store.dispatch('APIToken');
  const response = await fetch(`${API_URI}/hike/`, {
    method: 'POST',
    body: JSON.stringify(formatHikeToPost(hike)),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token.access_token}`
    },
  });
  if (response.ok) {
    const id = await response.text();
    await store.dispatch('addCreatedHike', id);
  }
  return response.ok;
}

/**
 * update a hike information
 * @param hike updated hike
 * @returns {Promise<boolean>}
 */
export async function updateHike(hike) {
  const token = await store.dispatch('APIToken');
  const response = await fetch(`${API_URI}/hike/${hike._id}`, {
    method: 'PUT',
    body: JSON.stringify(formatUpdatedHikeToPost(hike)),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token.access_token}`
    },
  });
  return response.ok;
}

/**
 * update the number of participant for a hike
 * @param hikeId
 * @param participants Array of members ID
 * @returns {Promise<boolean>}
 */
export async function updateHikeParticipants(hikeId, participants) {
  const token = await store.dispatch('APIToken');
  const response = await fetch(`${API_URI}/hike/${hikeId}`, {
    method: 'PUT',
    body: JSON.stringify({
      hike: {
        participants: participants
      }
    }),
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token.access_token}`
    },
  });
  return response.ok;
}

/**
 * fetch hikes from the given url
 * @param url API url
 * @returns {Promise<*[]|any>}
 */
async function fetchHikesFrom(url) {
  const token = await store.dispatch('APIToken');
  const response = await fetch(url, {headers: {'Authorization': `Bearer ${token.access_token}`}});
  if (!response.ok) {
    const errorMsg = await response.text()
    alert(errorMsg)
    return [];
  }
  return await response.json();
}

/**
 * fetch hikes with a given query (filter hikes with query)
 * @param query Query parameters object
 * @returns {Promise<{
 *   location: {lat: number, lng: number},
 *   hikes: *[]
 * }>}
 */
export async function fetchHikesWithQuery(query) {
  const url = API_URI + '/hike/search/place?' +
    `datemin=${query.datemin ? query.datemin : new Date().toISOString()}` +
    `&datemax=${query.datemax ? query.datemax : DEFAULT_QUERY.datemax}` +
    `&place=${query.place}&rad=${query.rad}&durmin=0&durmax=100` +
    `&distmin=${query.distmin}&distmax=${query.distmax}` +
    `&diffmin=${query.diffmin}&diffmax=${query.diffmax}`
  return fetchHikesFrom(url);
}

/**
 * fetch all hikes
 * @returns {Promise<*[]>} An array of hikes objects
 */
export async function fetchAllHikes() {
  return fetchHikesFrom(API_URI + '/hike');
}

/**
 * get the token to use for the API
 * @returns {Promise<{access_token: string}>}
 */
export async function getAPIToken() {
  let token = {access_token: ''}
  const response = await fetch(API_URI + '/token');
  if (response.ok) {
    token = await response.json();
  }
  return token;
}
