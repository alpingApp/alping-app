import Vue from 'vue'
import VueRouter from 'vue-router'
import HomePage from '../views/Home.vue'
import CreateHike from '../views/CreateHike.vue'
import SignUp from '../views/SignUp.vue'
import ProfilePage from '../views/Profile.vue'
import SearchPage from '../views/Search.vue'
import UserEdition from "../views/UserEdition";
import HikeEdition from "../views/HikeEdition";
import ErrorPage from "../views/ErrorPage";
import AboutPage from "../views/About.vue";
import {getAuth, onAuthStateChanged} from 'firebase/auth';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: HomePage
  },
  {
    path: '/newHike',
    name: 'CreateHike',
    component: CreateHike,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/signup',
    name: 'Signup',
    component: SignUp
  },
  {
    path: '/profile/:username',
    name: 'Profile',
    component: ProfilePage,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/search/query',
    name: 'Search',
    component: SearchPage,
  },
  {
    path: '/edition/user',
    name: 'UserEdition',
    component: UserEdition,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/edition/hike',
    name: 'HikeEdition',
    component: HikeEdition,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/error/',
    name: 'Error',
    component: ErrorPage,
  },
  {
    path: '/error/:errorNumber',
    name: 'ErrorNumber',
    component: ErrorPage,
  },
  {
    path: '/about',
    name: 'About',
    component: AboutPage,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    onAuthStateChanged(getAuth(), user => {
      if (user) {
        next();
      } else {
        router.push({name: 'Signup'});
      }
    });
  } else {
    next();
  }
});

export default router
