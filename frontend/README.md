# Alping - Frontend web app

![Pipeline](https://gitlab.com/alpingApp/alping-app/badges/main/pipeline.svg)

## Description

![Home page](../img/visual1.png)

The frontend part of Alping is a Vue.js web application that communicates with the bacendAPI in order to offer the users features that allow to create, edit and join hikes.

## Installation

**The Mongo database**

A local docker container with MongoDB needs to be set up.

3. Start by creating a `docker-compose.yaml` file containing the following code :

```yaml
version: '3.1'
services:
  mongo:
    image: mongo
    container_name: "mongo-alping"
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: admin
      MONGO_INITDB_ROOT_PASSWORD: alping
    volumes:
      - './dockervolume/mongodb:/data/db'
    ports:
      - 27017:27017
```

4. Then open a terminal where the `docker-compose.yaml` is located and run the following command to start the MongoDB container :

```bash
docker-compose up
```

**The backend API**

Once MongoDB is runnning, the Alping backend API can be started.

5. Open a terminal inside the `backend` folder.

6. Run the `npm install` command inside the folder

7. Start the backend server with the command `npm run start`. The server should start and automatically connect to MongoDB

**The frontend Vue.js app**

Finally, the frontend web app can be started

8. Open a terminal inside the backend folder

9. Run the `npm install` command inside the folder

10. Start the frontend server with the command `npm run serve`. The server should start and automatically and be available on [http://localhost:8080/](http://localhost:8080/) 

> Tip : the backend / frontend tests can be started with `npm run test`
