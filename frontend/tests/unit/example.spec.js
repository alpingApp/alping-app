import { shallowMount } from '@vue/test-utils'
import HomePage from '../../src/views/Home.vue'

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const wrapper = shallowMount(HomePage)
    expect(wrapper.find('.home').exists())
  })
})
